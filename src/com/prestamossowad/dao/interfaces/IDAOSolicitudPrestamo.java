package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.Usuario;

public interface IDAOSolicitudPrestamo {

	public SolicitudPrestamo getSolicitudPrestamo(int id);
	
	public List<SolicitudPrestamo> getAllSolicitudesPrestamo(Usuario objUsuario, boolean prestamista);
	
	public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestamista(Usuario objUsuario);
	
	public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestamista(Usuario objUsuario);
	
	public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestatario(Usuario objUsuario);
	
	public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestatario(Usuario objUsuario);
	
	public String insert(SolicitudPrestamo objSolicitudPrestamo, Usuario objUsuarioPrestamista, Usuario objUsuarioPrestatario);
	
	public boolean update(SolicitudPrestamo objSolicitudPrestamo);
	
	public boolean delete(SolicitudPrestamo objSolicitudPrestamo);
}
