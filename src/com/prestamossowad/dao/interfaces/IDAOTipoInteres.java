package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.TasaInteres;
import com.prestamossowad.dominio.TipoInteres;
import com.prestamossowad.dominio.Usuario;

public interface IDAOTipoInteres {

	public TipoInteres getTipoInteres(int id);
	
	public List<TipoInteres> getAllTiposInteres(Usuario objUsuario);
	
	public boolean insert(TasaInteres objTasaInteres, Usuario objUsuario);
	
	public boolean delete(TipoInteres objTipoInteres);
}
