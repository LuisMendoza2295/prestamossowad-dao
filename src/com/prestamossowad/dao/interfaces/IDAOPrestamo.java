package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.Cuota;
import com.prestamossowad.dominio.Prestamo;
import com.prestamossowad.dominio.Usuario;

public interface IDAOPrestamo {

	public Prestamo getPrestamo(int id);
	
	public Prestamo getPrestamo(Cuota objCuota);
	
	public boolean insert(Prestamo objPrestamo);
	
	public List<Prestamo> getAllPrestamosActivos(Usuario objUsuario, boolean prestamista);
}
