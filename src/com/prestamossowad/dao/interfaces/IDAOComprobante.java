package com.prestamossowad.dao.interfaces;

import com.prestamossowad.dominio.Comprobante;

public interface IDAOComprobante {

	public boolean insert(Comprobante objComprobante);
}
