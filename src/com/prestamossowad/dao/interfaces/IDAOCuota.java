package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.Cuota;
import com.prestamossowad.dominio.Prestamo;

public interface IDAOCuota {

	public Cuota getCuota(int id);
	
	public List<Cuota> getAllCuotas(Prestamo objPrestamo);
	
	public boolean insert(Prestamo objPrestamo);
	
	public String update(Cuota objCuota);
}
