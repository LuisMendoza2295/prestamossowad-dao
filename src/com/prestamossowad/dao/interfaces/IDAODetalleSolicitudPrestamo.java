package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.DetalleSolicitudPrestamo;

public interface IDAODetalleSolicitudPrestamo {

	public boolean insertDetalleSolicitudPrestamo(List<DetalleSolicitudPrestamo> lstDetalleSolicitudPrestamo);
}
