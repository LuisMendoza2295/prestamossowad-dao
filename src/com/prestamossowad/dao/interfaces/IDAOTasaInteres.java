package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.TasaInteres;
import com.prestamossowad.dominio.TipoInteres;
import com.prestamossowad.dominio.Usuario;

public interface IDAOTasaInteres {

	public TasaInteres getTasaInteres(int id);
	
	public TasaInteres getTasaInteres(TipoInteres objTipoInteres);
	
	public List<TasaInteres> getAllTasasInteres(Usuario objUsuario);
	
	public List<TasaInteres> getAllTasasInteres(TipoInteres objTipoInteres);
	
	public boolean insert(TasaInteres objTasaInteres);
}
