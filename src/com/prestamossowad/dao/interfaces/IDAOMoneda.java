package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.Moneda;

public interface IDAOMoneda {

	public Moneda getMoneda(int id);
	
	public List<Moneda> getAllMonedas();
}
