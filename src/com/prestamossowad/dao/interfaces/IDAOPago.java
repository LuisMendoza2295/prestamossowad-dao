package com.prestamossowad.dao.interfaces;

import com.prestamossowad.dominio.Pago;

public interface IDAOPago {

	public Pago getPago(int id);
	
	public boolean insert(Pago objPago);
}
