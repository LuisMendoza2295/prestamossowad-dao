package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.Usuario;

public interface IDAOUsuario {

	public Usuario getUsuario(int id);
	
	public Usuario getUsuario(String username, String password);
	
	public String insert(Usuario objUsuario);
	
	public List<Usuario> getAllUsuarios(Usuario objUsuario);
}
