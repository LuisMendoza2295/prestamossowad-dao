package com.prestamossowad.dao.interfaces;

import com.prestamossowad.dominio.SolicitudPrestamo;

public interface IDAODetalleTipoPago {

	public boolean insertAllDetalleTipoPago(SolicitudPrestamo objSolicitudPrestamo);
	
	public boolean delete(SolicitudPrestamo objSolicitudPrestamo);
}
