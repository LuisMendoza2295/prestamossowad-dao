package com.prestamossowad.dao.interfaces;

import java.util.List;

import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.TipoPago;

public interface IDAOTipoPago {

	public TipoPago getTipoPago(int id);
	
	public List<TipoPago> getAllTiposPago(SolicitudPrestamo objSolicitudPrestamo);
	
	public boolean insert(TipoPago objTipoPago);	
}
