package com.prestamossowad.dao.concrete;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOCuota;
import com.prestamossowad.dominio.Cuota;
import com.prestamossowad.dominio.Prestamo;

public class DAOCuota implements IDAOCuota{

	private static DAOCuota instance = null;

    private DAOCuota() {
    }

    public static DAOCuota getInstance() {
        if (instance == null) {
            instance = new DAOCuota();
        }

        return instance;
    }

	@Override
	public Cuota getCuota(int id) {
		Cuota objCuota = null;
		
		try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.monto, c.amortizacion, c.interes, "
        		+ "c.mora, c.fechaInicio, c.fechaFin, c.pagado, c.fechaRegistro, c.fechaModificacion, c.idPrestamo "
        		+ "FROM Cuotas c "
        		+ "WHERE id = ?;")){            
            preparedStatement.setInt(1, id);
            
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
            	objCuota = new Cuota(
                        resultSet.getInt("id"), 
                        resultSet.getBigDecimal("amortizacion"), 
                        resultSet.getBigDecimal("interes"), 
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("mora"),
                        resultSet.getDate("fechaInicio"), 
                        resultSet.getDate("fechaFin"), 
                        resultSet.getDate("fechaRegistro"), 
                        resultSet.getDate("fechaModificacion"),
                        resultSet.getBoolean("pagado"));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		return objCuota;
	}

	@Override
	public List<Cuota> getAllCuotas(Prestamo objPrestamo) {
		List<Cuota> lstCuotas = new ArrayList<>();
		
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.monto, c.amortizacion, c.interes, "
        		+ "c.mora, c.fechaInicio, c.fechaFin, c.pagado, c.fechaRegistro, c.fechaModificacion, c.idPrestamo "
        		+ "FROM Cuotas c "
        		+ "WHERE idPrestamo = ?;")){            
            preparedStatement.setInt(1, objPrestamo.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            Cuota objCuota;
            while (resultSet.next()){
                objCuota = new Cuota(
                        resultSet.getInt("id"), 
                        resultSet.getBigDecimal("amortizacion"), 
                        resultSet.getBigDecimal("interes"), 
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("mora"),
                        resultSet.getDate("fechaInicio"), 
                        resultSet.getDate("fechaFin"), 
                        resultSet.getDate("fechaRegistro"), 
                        resultSet.getDate("fechaModificacion"),
                        resultSet.getBoolean("pagado"));
                lstCuotas.add(objCuota);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lstCuotas;
	}

	@Override
	public boolean insert(Prestamo objPrestamo) {
		boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO Cuotas "
                + "(monto, amortizacion, interes, mora, fechaInicio, fechaFin, pagado, fechaRegistro, fechaModificacion, idPrestamo) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")) {
            for (Cuota objCuota : objPrestamo.getLstCuotas()) {
                preparedStatement.setBigDecimal(1, objCuota.getMonto());
                preparedStatement.setBigDecimal(2, objCuota.getAmortizacion());
                preparedStatement.setBigDecimal(3, objCuota.getInteres());
                preparedStatement.setBigDecimal(4, objCuota.getMora());
                preparedStatement.setDate(5, new java.sql.Date(objCuota.getFechaInicio().getTime()));
                preparedStatement.setDate(6, new java.sql.Date(objCuota.getFechaFin().getTime()));
                preparedStatement.setBoolean(7, objCuota.isPagado());
                preparedStatement.setDate(8, new java.sql.Date(objCuota.getFechaRegistro().getTime()));
                if (objCuota.getFechaModificacion() == null) {
                    preparedStatement.setNull(9, Types.DATE);
                } else {
                    preparedStatement.setDate(9, new java.sql.Date(objCuota.getFechaModificacion().getTime()));
                }
                preparedStatement.setInt(10, objPrestamo.getId());

                preparedStatement.execute();
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
	}

	@Override
	public String update(Cuota objCuota) {
		try (CallableStatement callableStatement = ConnectionManager.getConnection().prepareCall("{call sp_RegisterFeePayment(?)}")){            
            callableStatement.setInt(1, objCuota.getId()); 
            
            callableStatement.execute();
            
            return "Pago registrado correctamente!";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
	}
}
