package com.prestamossowad.dao.concrete;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOTipoInteres;
import com.prestamossowad.dominio.TasaInteres;
import com.prestamossowad.dominio.TipoInteres;
import com.prestamossowad.dominio.Usuario;

public class DAOTipoInteres implements IDAOTipoInteres{

private static DAOTipoInteres instance = null;
    
    private DAOTipoInteres(){
    }
    
    public static DAOTipoInteres getInstance(){
        if(instance == null){
            instance = new DAOTipoInteres();
        }
        
        return instance;
    }
    
    public TipoInteres getTipoInteres(int id){
        TipoInteres objTipoInteres = null;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.nombre, "
                + "ti.diasPlazos, ti.fechaRegistro, ti.fechaModificacion, ti.estado, ti.idUsuario "
                + "FROM TiposInteres ti "
                + "WHERE ti.id = ?")){
            preparedStatement.setInt(1, id);
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                objTipoInteres = new TipoInteres(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getInt("diasPlazos"),
                        resultSet.getBoolean("estado"),
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getDate("fechaModificacion"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        /*if(objTipoInteres != null){
        	objTipoInteres.setObjTasaInteres(DAOTasaInteres.getInstance().getTasaInteres(objTipoInteres));
        }*/
        
        return objTipoInteres;
    }
    
    public List<TipoInteres> getAllTiposInteres(Usuario objUsuario){
        List<TipoInteres> lstTiposInteres = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.nombre, "
                + "ti.diasPlazos, ti.fechaRegistro, ti.fechaModificacion, ti.estado, ti.idUsuario "
                + "FROM TiposInteres ti "
                + "WHERE ti.idUsuario = ?;")){
            preparedStatement.setInt(1, objUsuario.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                TipoInteres objTipoInteres = new TipoInteres(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getInt("diasPlazos"),
                        resultSet.getBoolean("estado"),
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getDate("fechaModificacion"));
                lstTiposInteres.add(objTipoInteres);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        /*if(!lstTiposInteres.isEmpty()){
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            
            int size = lstTiposInteres.size();
            for(int i = 0; i < size; i++){
                lstTiposInteres.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstTiposInteres.get(i)));
            }
        }*/
        
        return lstTiposInteres;
    }

	@Override
	public boolean insert(TasaInteres objTasaInteres, Usuario objUsuario) {
		boolean result = false;
		
		try(CallableStatement callableStatement = ConnectionManager.getConnection().prepareCall("{? = call sp_RegisterInterestType(?, ?, ?, ?, ?)}")){
			callableStatement.registerOutParameter(1, Types.INTEGER);
			callableStatement.setString(2, objTasaInteres.getObjTipoInteres().getNombre());
			callableStatement.setInt(3, objTasaInteres.getObjTipoInteres().getDiasPlazos());
			callableStatement.setBigDecimal(4, objTasaInteres.getTea());
			callableStatement.setBigDecimal(5, objTasaInteres.getTim());
			callableStatement.setInt(6, objUsuario.getId());
			
			callableStatement.execute();
			
			objTasaInteres.getObjTipoInteres().setId(callableStatement.getInt(1));
			
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public boolean delete(TipoInteres objTipoInteres) {
		boolean result = false;
		
		try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE TiposInteres SET estado = 0, fechaModificacion = GETDATE() WHERE id = ?;")){
			preparedStatement.setInt(1, objTipoInteres.getId());
			
			preparedStatement.execute();
			
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
}
