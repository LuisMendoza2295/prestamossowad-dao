package com.prestamossowad.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOComprobante;
import com.prestamossowad.dominio.Comprobante;

public class DAOComprobante implements IDAOComprobante{

	private static DAOComprobante instance = null;
	
	private DAOComprobante() {
	}
	
	public static DAOComprobante getInstance(){
		if(instance == null){
			instance = new DAOComprobante();
		}
		
		return instance;
	}
	
	@Override
	public boolean insert(Comprobante objComprobante) {
		boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO Comprobantes "
        		+ "(codigo, fechaRegistro, idPago) "
        		+ "VALUES (?, ?, ?)",PreparedStatement.RETURN_GENERATED_KEYS)){
            
            preparedStatement.setString(1, objComprobante.getCodigo());
            preparedStatement.setDate(2, new java.sql.Date(objComprobante.getFechaRegistro().getTime()));
            preparedStatement.setInt(3, objComprobante.getObjPago().getId());
            
            result = preparedStatement.execute();
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            
            while (keySet.next()){
                objComprobante.setId(keySet.getInt(1));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
	}
}
