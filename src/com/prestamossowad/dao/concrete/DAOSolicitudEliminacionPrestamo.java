package com.prestamossowad.dao.concrete;

import com.prestamossowad.dao.interfaces.IDAOSolicitudEliminacionPrestamo;

public class DAOSolicitudEliminacionPrestamo implements IDAOSolicitudEliminacionPrestamo{

	private static DAOSolicitudEliminacionPrestamo instance = null;
    
    private DAOSolicitudEliminacionPrestamo(){
    }
    
    public static DAOSolicitudEliminacionPrestamo getInstance(){
        if(instance == null){
            instance = new DAOSolicitudEliminacionPrestamo();
        }
        
        return instance;
    }
}
