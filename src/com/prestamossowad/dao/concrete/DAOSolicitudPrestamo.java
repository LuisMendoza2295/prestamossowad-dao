package com.prestamossowad.dao.concrete;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOSolicitudPrestamo;
import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.TipoPago;
import com.prestamossowad.dominio.Usuario;

public class DAOSolicitudPrestamo implements IDAOSolicitudPrestamo{
	
	private static DAOSolicitudPrestamo instance = null;

    private DAOSolicitudPrestamo() {
    }

    public static DAOSolicitudPrestamo getInstance() {
        if (instance == null) {
            instance = new DAOSolicitudPrestamo();
        }

        return instance;
    }

    @Override
    public SolicitudPrestamo getSolicitudPrestamo(int id) {
        SolicitudPrestamo objSolicitudPrestamo = null;
        int idUsuarioPrestamista = 0;
        int idUsuarioPrestatario = 0;
        int idMoneda = 0;
        int idTasaInteres = 0;
        int idUsuarioModificacion = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.monto, "
                + "sp.cuotas, sp.estado, sp.aprobadoPrestamista, sp.aprobadoPrestatario, sp.fechaRegistro, sp.fechaModificacion, "
                + "sp.idTasaInteres, sp.idMoneda, sp.idUsuarioModificacion, dsp.idUsuario, dsp.prestamista "
                + "FROM SolicitudesPrestamo sp INNER JOIN DetalleSolicitudPrestamo dsp ON sp.id = dsp.idSolicitudPrestamo "
                + "INNER JOIN usuarios u ON u.id = dsp.idUsuario "
                + "WHERE sp.id = ? AND sp.estado = 1;")) {
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            boolean prestamista;
            while (resultSet.next()) {
                prestamista = resultSet.getBoolean("prestamista");
                if (prestamista) {
                    idUsuarioPrestamista = resultSet.getInt("idUsuario");
                } else {
                    idUsuarioPrestatario = resultSet.getInt("idUsuario");
                }
                if (objSolicitudPrestamo == null) {
                    idMoneda = resultSet.getInt("idMoneda");
                    idTasaInteres = resultSet.getInt("idTasaInteres");
                    idUsuarioModificacion = resultSet.getInt("idUsuarioModificacion");
                    objSolicitudPrestamo = new SolicitudPrestamo(
                            resultSet.getInt("id"),
                            resultSet.getBigDecimal("monto"),
                            resultSet.getInt("aprobadoPrestamista") == 1,
                            resultSet.getInt("aprobadoPrestatario") == 1,
                            resultSet.getInt("cuotas"),
                            resultSet.getInt("estado") == 1,
                            resultSet.getDate("fechaRegistro"),
                            resultSet.getDate("fechaModificacion"),
                            null,
                            null,
                            null,
                            null,
                            new ArrayList<TipoPago>(),
                            null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitudPrestamo != null) {
            objSolicitudPrestamo.setObjMoneda(DAOMoneda.getInstance().getMoneda(idMoneda));
            objSolicitudPrestamo.setObjTasaInteres(DAOTasaInteres.getInstance().getTasaInteres(idTasaInteres));
            objSolicitudPrestamo.setObjUsuarioPrestamista(DAOUsuario.getInstance().getUsuario(idUsuarioPrestamista));
            objSolicitudPrestamo.setObjUsuarioPrestatario(DAOUsuario.getInstance().getUsuario(idUsuarioPrestatario));
            objSolicitudPrestamo.setLstTiposPago(DAOTipoPago.getInstance().getAllTiposPago(objSolicitudPrestamo));
            objSolicitudPrestamo.setObjUsuarioModificacion(DAOUsuario.getInstance().getUsuario(idUsuarioModificacion));
        }

        return objSolicitudPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamo(Usuario objUsuario, boolean prestamista) {
        Map<Integer, SolicitudPrestamo> mapSolicitudesPrestamos = new HashMap<>();
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        Map<Integer, Integer> mapIdsTasasInteres = new HashMap<>();
        Map<Integer, Integer> mapIdsMonedas = new HashMap<>();
        Map<Integer, Integer> mapIdsUsuariosModificacion = new HashMap<>();
        Map<Integer, Map<Integer, Boolean>> mapIdsUsuarios = new HashMap<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.monto, "
                + "sp.cuotas, sp.estado, sp.aprobadoPrestamista, sp.aprobadoPrestatario, sp.fechaRegistro, sp.fechaModificacion, "
                + "sp.idTasaInteres, sp.idMoneda, sp.idUsuarioModificacion, dsp.idUsuario, dsp.prestamista "
                + "FROM (SELECT spsub.id, spsub.monto, "
                + "	spsub.cuotas, spsub.estado, spsub.aprobadoPrestamista, spsub.aprobadoPrestatario, spsub.fechaRegistro, spsub.fechaModificacion, "
                + "	spsub.idTasaInteres, spsub.idMoneda, spsub.idUsuarioModificacion, dspsub.idUsuario, dspsub.prestamista "
                + "	FROM SolicitudesPrestamo spsub INNER JOIN DetalleSolicitudPrestamo dspsub ON spsub.id = dspsub.idSolicitudPrestamo "
                + "	INNER JOIN usuarios usub ON usub.id = dspsub.idUsuario "
                + "	WHERE dspsub.idUsuario = ? AND dspsub.prestamista = ? AND spsub.estado = 1) sp INNER JOIN DetalleSolicitudPrestamo dsp ON sp.id = dsp.idSolicitudPrestamo;")) {
            preparedStatement.setInt(1, objUsuario.getId());
            preparedStatement.setBoolean(2, prestamista);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("aprobadoPrestamista") == 1,
                        resultSet.getInt("aprobadoPrestatario") == 1,
                        resultSet.getInt("cuotas"),
                        resultSet.getInt("estado") == 1,
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getDate("fechaModificacion"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<TipoPago>(),
                        null);
                if (mapSolicitudesPrestamos.get(objSolicitudPrestamo.getId()) == null) {
                    mapSolicitudesPrestamos.put(objSolicitudPrestamo.getId(), objSolicitudPrestamo);
                    mapIdsMonedas.put(objSolicitudPrestamo.getId(), resultSet.getInt("idMoneda"));
                    mapIdsTasasInteres.put(objSolicitudPrestamo.getId(), resultSet.getInt("idTasaInteres"));
                    mapIdsUsuariosModificacion.put(objSolicitudPrestamo.getId(), resultSet.getInt("idUsuarioModificacion"));
                }

                Map<Integer, Boolean> mapDetalle = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                if (mapDetalle == null) {
                    mapDetalle = new HashMap<>();
                }
                mapDetalle.put(resultSet.getInt("idUsuario"), resultSet.getInt("prestamista") == 1);
                mapIdsUsuarios.put(objSolicitudPrestamo.getId(), mapDetalle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!mapSolicitudesPrestamos.isEmpty()) {
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            for (Map.Entry<Integer, SolicitudPrestamo> entry : mapSolicitudesPrestamos.entrySet()) {
                SolicitudPrestamo objSolicitudPrestamo = entry.getValue();
                objSolicitudPrestamo.setObjMoneda(objDAOMoneda.getMoneda(mapIdsMonedas.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setObjTasaInteres(objDAOTasaInteres.getTasaInteres(mapIdsTasasInteres.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setLstTiposPago(objDAOTipoPago.getAllTiposPago(objSolicitudPrestamo));
                objSolicitudPrestamo.setObjUsuarioModificacion(objDAOUsuario.getUsuario(mapIdsUsuariosModificacion.get(objSolicitudPrestamo.getId())));
                Map<Integer, Boolean> mapDetail = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                for (Map.Entry<Integer, Boolean> entryDetail : mapDetail.entrySet()) {
                    if (entryDetail.getValue()) {
                        objSolicitudPrestamo.setObjUsuarioPrestamista(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    } else {
                        objSolicitudPrestamo.setObjUsuarioPrestatario(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    }
                }
            }
        }

        Collection<SolicitudPrestamo> collection = mapSolicitudesPrestamos.values();

        lstSolicitudesPrestamo = new ArrayList<>(collection);

        return lstSolicitudesPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestamista(Usuario objUsuario) {
        Map<Integer, SolicitudPrestamo> mapSolicitudesPrestamos = new HashMap<>();
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        Map<Integer, Integer> mapIdsTasasInteres = new HashMap<>();
        Map<Integer, Integer> mapIdsMonedas = new HashMap<>();
        Map<Integer, Integer> mapIdsUsuariosModificacion = new HashMap<>();
        Map<Integer, Map<Integer, Boolean>> mapIdsUsuarios = new HashMap<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.monto, "
                + "sp.cuotas, sp.estado, sp.aprobadoPrestamista, sp.aprobadoPrestatario, sp.fechaRegistro, sp.fechaModificacion, "
                + "sp.idTasaInteres, sp.idMoneda, sp.idUsuarioModificacion, dsp.idUsuario, dsp.prestamista "
                + "FROM (SELECT spsub.id, spsub.monto, "
                + "	spsub.cuotas, spsub.estado, spsub.aprobadoPrestamista, spsub.aprobadoPrestatario, spsub.fechaRegistro, spsub.fechaModificacion, "
                + "	spsub.idTasaInteres, spsub.idMoneda, spsub.idUsuarioModificacion, dspsub.idUsuario, dspsub.prestamista "
                + "	FROM SolicitudesPrestamo spsub INNER JOIN DetalleSolicitudPrestamo dspsub ON spsub.id = dspsub.idSolicitudPrestamo "
                + "	INNER JOIN usuarios usub ON usub.id = dspsub.idUsuario "
                + "	WHERE dspsub.idUsuario = ? AND dspsub.prestamista = 1 AND aprobadoPrestamista = 0 AND spsub.estado = 1 AND (spsub.idUsuarioModificacion <> ? OR spsub.idUsuarioModificacion IS NULL)) sp "
                + "INNER JOIN DetalleSolicitudPrestamo dsp ON sp.id = dsp.idSolicitudPrestamo;")) {
            preparedStatement.setInt(1, objUsuario.getId());
            preparedStatement.setInt(2, objUsuario.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("aprobadoPrestamista") == 1,
                        resultSet.getInt("aprobadoPrestatario") == 1,
                        resultSet.getInt("cuotas"),
                        resultSet.getInt("estado") == 1,
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getDate("fechaModificacion"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<TipoPago>(),
                        null);
                if (mapSolicitudesPrestamos.get(objSolicitudPrestamo.getId()) == null) {
                    mapSolicitudesPrestamos.put(objSolicitudPrestamo.getId(), objSolicitudPrestamo);
                    mapIdsMonedas.put(objSolicitudPrestamo.getId(), resultSet.getInt("idMoneda"));
                    mapIdsTasasInteres.put(objSolicitudPrestamo.getId(), resultSet.getInt("idTasaInteres"));
                    mapIdsUsuariosModificacion.put(objSolicitudPrestamo.getId(), resultSet.getInt("idUsuarioModificacion"));
                }

                Map<Integer, Boolean> mapDetalle = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                if (mapDetalle == null) {
                    mapDetalle = new HashMap<>();
                }
                mapDetalle.put(resultSet.getInt("idUsuario"), resultSet.getInt("prestamista") == 1);
                mapIdsUsuarios.put(objSolicitudPrestamo.getId(), mapDetalle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!mapSolicitudesPrestamos.isEmpty()) {
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            for (Map.Entry<Integer, SolicitudPrestamo> entry : mapSolicitudesPrestamos.entrySet()) {
                SolicitudPrestamo objSolicitudPrestamo = entry.getValue();
                objSolicitudPrestamo.setObjMoneda(objDAOMoneda.getMoneda(mapIdsMonedas.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setObjTasaInteres(objDAOTasaInteres.getTasaInteres(mapIdsTasasInteres.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setLstTiposPago(objDAOTipoPago.getAllTiposPago(objSolicitudPrestamo));
                objSolicitudPrestamo.setObjUsuarioModificacion(objDAOUsuario.getUsuario(mapIdsUsuariosModificacion.get(objSolicitudPrestamo.getId())));
                Map<Integer, Boolean> mapDetail = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                for (Map.Entry<Integer, Boolean> entryDetail : mapDetail.entrySet()) {
                    if (entryDetail.getValue()) {
                        objSolicitudPrestamo.setObjUsuarioPrestamista(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    } else {
                        objSolicitudPrestamo.setObjUsuarioPrestatario(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    }
                }
            }
        }

        Collection<SolicitudPrestamo> collection = mapSolicitudesPrestamos.values();

        lstSolicitudesPrestamo = new ArrayList<>(collection);

        return lstSolicitudesPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestamista(Usuario objUsuario) {
        Map<Integer, SolicitudPrestamo> mapSolicitudesPrestamos = new HashMap<>();
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        Map<Integer, Integer> mapIdsTasasInteres = new HashMap<>();
        Map<Integer, Integer> mapIdsMonedas = new HashMap<>();
        Map<Integer, Integer> mapIdsUsuariosModificacion = new HashMap<>();
        Map<Integer, Map<Integer, Boolean>> mapIdsUsuarios = new HashMap<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.monto, "
                + "sp.cuotas, sp.estado, sp.aprobadoPrestamista, sp.aprobadoPrestatario, sp.fechaRegistro, sp.fechaModificacion, "
                + "sp.idTasaInteres, sp.idMoneda, sp.idUsuarioModificacion, dsp.idUsuario, dsp.prestamista "
                + "FROM (SELECT spsub.id, spsub.monto, "
                + "	spsub.cuotas, spsub.estado, spsub.aprobadoPrestamista, spsub.aprobadoPrestatario, spsub.fechaRegistro, spsub.fechaModificacion, "
                + "	spsub.idTasaInteres, spsub.idMoneda, spsub.idUsuarioModificacion, dspsub.idUsuario, dspsub.prestamista "
                + "	FROM SolicitudesPrestamo spsub INNER JOIN DetalleSolicitudPrestamo dspsub ON spsub.id = dspsub.idSolicitudPrestamo "
                + "	INNER JOIN usuarios usub ON usub.id = dspsub.idUsuario "
                + "	WHERE dspsub.idUsuario = ? AND dspsub.prestamista = 1 AND aprobadoPrestatario = 0 AND spsub.estado = 1 AND (spsub.idUsuarioModificacion <> ? OR spsub.idUsuarioModificacion IS NULL)) sp "
                + "INNER JOIN DetalleSolicitudPrestamo dsp ON sp.id = dsp.idSolicitudPrestamo;")) {
            preparedStatement.setInt(1, objUsuario.getId());
            preparedStatement.setInt(2, objUsuario.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("aprobadoPrestamista") == 1,
                        resultSet.getInt("aprobadoPrestatario") == 1,
                        resultSet.getInt("cuotas"),
                        resultSet.getInt("estado") == 1,
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getDate("fechaModificacion"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<TipoPago>(),
                        null);
                if (mapSolicitudesPrestamos.get(objSolicitudPrestamo.getId()) == null) {
                    mapSolicitudesPrestamos.put(objSolicitudPrestamo.getId(), objSolicitudPrestamo);
                    mapIdsMonedas.put(objSolicitudPrestamo.getId(), resultSet.getInt("idMoneda"));
                    mapIdsTasasInteres.put(objSolicitudPrestamo.getId(), resultSet.getInt("idTasaInteres"));
                    mapIdsUsuariosModificacion.put(objSolicitudPrestamo.getId(), resultSet.getInt("idUsuarioModificacion"));
                }

                Map<Integer, Boolean> mapDetalle = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                if (mapDetalle == null) {
                    mapDetalle = new HashMap<>();
                }
                mapDetalle.put(resultSet.getInt("idUsuario"), resultSet.getInt("prestamista") == 1);
                mapIdsUsuarios.put(objSolicitudPrestamo.getId(), mapDetalle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!mapSolicitudesPrestamos.isEmpty()) {
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            for (Map.Entry<Integer, SolicitudPrestamo> entry : mapSolicitudesPrestamos.entrySet()) {
                SolicitudPrestamo objSolicitudPrestamo = entry.getValue();
                objSolicitudPrestamo.setObjMoneda(objDAOMoneda.getMoneda(mapIdsMonedas.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setObjTasaInteres(objDAOTasaInteres.getTasaInteres(mapIdsTasasInteres.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setLstTiposPago(objDAOTipoPago.getAllTiposPago(objSolicitudPrestamo));
                objSolicitudPrestamo.setObjUsuarioModificacion(objDAOUsuario.getUsuario(mapIdsUsuariosModificacion.get(objSolicitudPrestamo.getId())));
                Map<Integer, Boolean> mapDetail = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                for (Map.Entry<Integer, Boolean> entryDetail : mapDetail.entrySet()) {
                    if (entryDetail.getValue()) {
                        objSolicitudPrestamo.setObjUsuarioPrestamista(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    } else {
                        objSolicitudPrestamo.setObjUsuarioPrestatario(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    }
                }
            }
        }

        Collection<SolicitudPrestamo> collection = mapSolicitudesPrestamos.values();

        lstSolicitudesPrestamo = new ArrayList<>(collection);

        return lstSolicitudesPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestatario(Usuario objUsuario) {
        Map<Integer, SolicitudPrestamo> mapSolicitudesPrestamos = new HashMap<>();
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        Map<Integer, Integer> mapIdsTasasInteres = new HashMap<>();
        Map<Integer, Integer> mapIdsMonedas = new HashMap<>();
        Map<Integer, Integer> mapIdsUsuariosModificacion = new HashMap<>();
        Map<Integer, Map<Integer, Boolean>> mapIdsUsuarios = new HashMap<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.monto, "
                + "sp.cuotas, sp.estado, sp.aprobadoPrestamista, sp.aprobadoPrestatario, sp.fechaRegistro, sp.fechaModificacion, "
                + "sp.idTasaInteres, sp.idMoneda, sp.idUsuarioModificacion, dsp.idUsuario, dsp.prestamista "
                + "FROM (SELECT spsub.id, spsub.monto, "
                + "	spsub.cuotas, spsub.estado, spsub.aprobadoPrestamista, spsub.aprobadoPrestatario, spsub.fechaRegistro, spsub.fechaModificacion, "
                + "	spsub.idTasaInteres, spsub.idMoneda, spsub.idUsuarioModificacion, dspsub.idUsuario, dspsub.prestamista "
                + "	FROM SolicitudesPrestamo spsub INNER JOIN DetalleSolicitudPrestamo dspsub ON spsub.id = dspsub.idSolicitudPrestamo "
                + "	INNER JOIN usuarios usub ON usub.id = dspsub.idUsuario "
                + "	WHERE dspsub.idUsuario = ? AND dspsub.prestamista = 0 AND aprobadoPrestamista = 0 AND spsub.estado = 1 AND (spsub.idUsuarioModificacion <> ? OR spsub.idUsuarioModificacion IS NULL)) sp "
                + "INNER JOIN DetalleSolicitudPrestamo dsp ON sp.id = dsp.idSolicitudPrestamo;")) {
            preparedStatement.setInt(1, objUsuario.getId());
            preparedStatement.setInt(2, objUsuario.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("aprobadoPrestamista") == 1,
                        resultSet.getInt("aprobadoPrestatario") == 1,
                        resultSet.getInt("cuotas"),
                        resultSet.getInt("estado") == 1,
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getDate("fechaModificacion"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<TipoPago>(),
                        null);
                if (mapSolicitudesPrestamos.get(objSolicitudPrestamo.getId()) == null) {
                    mapSolicitudesPrestamos.put(objSolicitudPrestamo.getId(), objSolicitudPrestamo);
                    mapIdsMonedas.put(objSolicitudPrestamo.getId(), resultSet.getInt("idMoneda"));
                    mapIdsTasasInteres.put(objSolicitudPrestamo.getId(), resultSet.getInt("idTasaInteres"));
                    mapIdsUsuariosModificacion.put(objSolicitudPrestamo.getId(), resultSet.getInt("idUsuarioModificacion"));
                }

                Map<Integer, Boolean> mapDetalle = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                if (mapDetalle == null) {
                    mapDetalle = new HashMap<>();
                }
                mapDetalle.put(resultSet.getInt("idUsuario"), resultSet.getInt("prestamista") == 1);
                mapIdsUsuarios.put(objSolicitudPrestamo.getId(), mapDetalle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!mapSolicitudesPrestamos.isEmpty()) {
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            for (Map.Entry<Integer, SolicitudPrestamo> entry : mapSolicitudesPrestamos.entrySet()) {
                SolicitudPrestamo objSolicitudPrestamo = entry.getValue();
                objSolicitudPrestamo.setObjMoneda(objDAOMoneda.getMoneda(mapIdsMonedas.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setObjTasaInteres(objDAOTasaInteres.getTasaInteres(mapIdsTasasInteres.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setLstTiposPago(objDAOTipoPago.getAllTiposPago(objSolicitudPrestamo));
                objSolicitudPrestamo.setObjUsuarioModificacion(objDAOUsuario.getUsuario(mapIdsUsuariosModificacion.get(objSolicitudPrestamo.getId())));
                Map<Integer, Boolean> mapDetail = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                for (Map.Entry<Integer, Boolean> entryDetail : mapDetail.entrySet()) {
                    if (entryDetail.getValue()) {
                        objSolicitudPrestamo.setObjUsuarioPrestamista(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    } else {
                        objSolicitudPrestamo.setObjUsuarioPrestatario(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    }
                }
            }
        }

        Collection<SolicitudPrestamo> collection = mapSolicitudesPrestamos.values();

        lstSolicitudesPrestamo = new ArrayList<>(collection);

        return lstSolicitudesPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestatario(Usuario objUsuario) {
        Map<Integer, SolicitudPrestamo> mapSolicitudesPrestamos = new HashMap<>();
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        Map<Integer, Integer> mapIdsTasasInteres = new HashMap<>();
        Map<Integer, Integer> mapIdsMonedas = new HashMap<>();
        Map<Integer, Integer> mapIdsUsuariosModificacion = new HashMap<>();
        Map<Integer, Map<Integer, Boolean>> mapIdsUsuarios = new HashMap<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.monto, "
                + "sp.cuotas, sp.estado, sp.aprobadoPrestamista, sp.aprobadoPrestatario, sp.fechaRegistro, sp.fechaModificacion, "
                + "sp.idTasaInteres, sp.idMoneda, sp.idUsuarioModificacion, dsp.idUsuario, dsp.prestamista "
                + "FROM (SELECT spsub.id, spsub.monto, "
                + "	spsub.cuotas, spsub.estado, spsub.aprobadoPrestamista, spsub.aprobadoPrestatario, spsub.fechaRegistro, spsub.fechaModificacion, "
                + "	spsub.idTasaInteres, spsub.idMoneda, spsub.idUsuarioModificacion, dspsub.idUsuario, dspsub.prestamista "
                + "	FROM SolicitudesPrestamo spsub INNER JOIN DetalleSolicitudPrestamo dspsub ON spsub.id = dspsub.idSolicitudPrestamo "
                + "	INNER JOIN usuarios usub ON usub.id = dspsub.idUsuario "
                + "	WHERE dspsub.idUsuario = ? AND dspsub.prestamista = 0 AND aprobadoPrestatario = 0 AND spsub.estado = 1 AND (spsub.idUsuarioModificacion <> ? OR spsub.idUsuarioModificacion IS NULL)) sp "
                + "INNER JOIN DetalleSolicitudPrestamo dsp ON sp.id = dsp.idSolicitudPrestamo;")) {
            preparedStatement.setInt(1, objUsuario.getId());
            preparedStatement.setInt(2, objUsuario.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("aprobadoPrestamista") == 1,
                        resultSet.getInt("aprobadoPrestatario") == 1,
                        resultSet.getInt("cuotas"),
                        resultSet.getInt("estado") == 1,
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getDate("fechaModificacion"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<TipoPago>(),
                        null);
                if (mapSolicitudesPrestamos.get(objSolicitudPrestamo.getId()) == null) {
                    mapSolicitudesPrestamos.put(objSolicitudPrestamo.getId(), objSolicitudPrestamo);
                    mapIdsMonedas.put(objSolicitudPrestamo.getId(), resultSet.getInt("idMoneda"));
                    mapIdsTasasInteres.put(objSolicitudPrestamo.getId(), resultSet.getInt("idTasaInteres"));
                    mapIdsUsuariosModificacion.put(objSolicitudPrestamo.getId(), resultSet.getInt("idUsuarioModificacion"));
                }

                Map<Integer, Boolean> mapDetalle = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                if (mapDetalle == null) {
                    mapDetalle = new HashMap<>();
                }
                mapDetalle.put(resultSet.getInt("idUsuario"), resultSet.getInt("prestamista") == 1);
                mapIdsUsuarios.put(objSolicitudPrestamo.getId(), mapDetalle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!mapSolicitudesPrestamos.isEmpty()) {
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            for (Map.Entry<Integer, SolicitudPrestamo> entry : mapSolicitudesPrestamos.entrySet()) {
                SolicitudPrestamo objSolicitudPrestamo = entry.getValue();
                objSolicitudPrestamo.setObjMoneda(objDAOMoneda.getMoneda(mapIdsMonedas.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setObjTasaInteres(objDAOTasaInteres.getTasaInteres(mapIdsTasasInteres.get(objSolicitudPrestamo.getId())));
                objSolicitudPrestamo.setLstTiposPago(objDAOTipoPago.getAllTiposPago(objSolicitudPrestamo));
                objSolicitudPrestamo.setObjUsuarioModificacion(objDAOUsuario.getUsuario(mapIdsUsuariosModificacion.get(objSolicitudPrestamo.getId())));
                Map<Integer, Boolean> mapDetail = mapIdsUsuarios.get(objSolicitudPrestamo.getId());
                for (Map.Entry<Integer, Boolean> entryDetail : mapDetail.entrySet()) {
                    if (entryDetail.getValue()) {
                        objSolicitudPrestamo.setObjUsuarioPrestamista(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    } else {
                        objSolicitudPrestamo.setObjUsuarioPrestatario(objDAOUsuario.getUsuario(entryDetail.getKey()));
                    }
                }
            }
        }

        Collection<SolicitudPrestamo> collection = mapSolicitudesPrestamos.values();

        lstSolicitudesPrestamo = new ArrayList<>(collection);

        return lstSolicitudesPrestamo;
    }

    @Override
    public String insert(SolicitudPrestamo objSolicitudPrestamo, Usuario objUsuarioPrestamista, Usuario objUsuarioPrestatario) {
    	try (CallableStatement callableStatement = ConnectionManager.getConnection().prepareCall("{? = call sp_RegisterLoanRequest(?, ?, ?, ?, ?, ?)}")){
            
            callableStatement.registerOutParameter(1, Types.INTEGER);
            callableStatement.setBigDecimal(2, objSolicitudPrestamo.getMonto());
            callableStatement.setInt(3, objSolicitudPrestamo.getCuotas());
            callableStatement.setInt(4, objSolicitudPrestamo.getObjTasaInteres().getId());
            callableStatement.setInt(5, objSolicitudPrestamo.getObjMoneda().getId());
            callableStatement.setInt(6, objUsuarioPrestamista.getId());
            callableStatement.setInt(7, objUsuarioPrestatario.getId());    
            
            callableStatement.execute();
            
            objSolicitudPrestamo.setId(callableStatement.getInt(1));
            
            return "Solicitud registrada correctamente!";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @Override
    public boolean update(SolicitudPrestamo objSolicitudPrestamo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE SolicitudesPrestamo "
                + "SET monto = ?, cuotas = ?, aprobadoPrestamista = ?, aprobadoPrestatario = ?, estado = ?, fechaRegistro = ?, fechaModificacion = ?, "
                + "idTasaInteres = ?, idMoneda = ?, idUsuarioModificacion = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setBigDecimal(1, objSolicitudPrestamo.getMonto());
            preparedStatement.setInt(2, objSolicitudPrestamo.getCuotas());
            preparedStatement.setInt(3, objSolicitudPrestamo.isAprobadoPrestamista() ? 1 : 0);
            preparedStatement.setInt(4, objSolicitudPrestamo.isAprobadoPrestatario() ? 1 : 0);
            preparedStatement.setInt(5, objSolicitudPrestamo.isEstado() ? 1 : 0);
            preparedStatement.setDate(6, new java.sql.Date(objSolicitudPrestamo.getFechaRegistro().getTime()));
            if (objSolicitudPrestamo.getFechaModificacion() == null) {
                preparedStatement.setNull(7, Types.DATE);
            } else {
                preparedStatement.setDate(7, new java.sql.Date(objSolicitudPrestamo.getFechaModificacion().getTime()));
            }
            preparedStatement.setInt(8, objSolicitudPrestamo.getObjTasaInteres().getId());
            preparedStatement.setInt(9, objSolicitudPrestamo.getObjMoneda().getId());
            preparedStatement.setInt(10, objSolicitudPrestamo.getObjUsuarioModificacion().getId());
            preparedStatement.setInt(11, objSolicitudPrestamo.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean delete(SolicitudPrestamo objSolicitudPrestamo) {
    	boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE SolicitudesPrestamo "
                + "SET estado = false "
                + "WHERE id = ?;")) {
            preparedStatement.setInt(1, objSolicitudPrestamo.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
