package com.prestamossowad.dao.concrete;

import java.sql.PreparedStatement;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAODetalleTipoPago;
import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.TipoPago;

public class DAODetalleTipoPago implements IDAODetalleTipoPago{

	private static DAODetalleTipoPago instance = null;
	
	private DAODetalleTipoPago(){
	}
	
	public static DAODetalleTipoPago getInstance(){
		if(instance == null){
			instance = new DAODetalleTipoPago();
		}
		
		return instance;
	}

	@Override
	public boolean insertAllDetalleTipoPago(SolicitudPrestamo objSolicitudPrestamo) {
		boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO DetalleTiposPago "
                + "(idSolicitudPrestamo, idTipoPago) "
                + "VALUES(?, ?);")){
                
            for(TipoPago objTipoPago : objSolicitudPrestamo.getLstTiposPago()){
                preparedStatement.setInt(1, objSolicitudPrestamo.getId());
                preparedStatement.setInt(2, objTipoPago.getId());
                
                preparedStatement.execute();
            }
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
	}

	@Override
	public boolean delete(SolicitudPrestamo objSolicitudPrestamo) {
		boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM DetalleTiposPago "
                + "WHERE idSolicitudPrestamo = ?;")){
            preparedStatement.setInt(1, objSolicitudPrestamo.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
	}
}
