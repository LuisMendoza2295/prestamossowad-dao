package com.prestamossowad.dao.concrete;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOTasaInteres;
import com.prestamossowad.dominio.TasaInteres;
import com.prestamossowad.dominio.TipoInteres;
import com.prestamossowad.dominio.Usuario;

public class DAOTasaInteres implements IDAOTasaInteres{

	private static DAOTasaInteres instance = null;

    private DAOTasaInteres() {
    }

    public static DAOTasaInteres getInstance() {
        if (instance == null) {
            instance = new DAOTasaInteres();
        }

        return instance;
    }

    @Override
    public TasaInteres getTasaInteres(int id) {
        TasaInteres objTasaInteres = null;
        int idTipoInteres = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.estado, "
                + "ti.tea, ti.tim, ti.fechaRegistro, ti.idTipoInteres "
                + "FROM TasasInteres ti "
                + "WHERE ti.id = ?;")) {
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoInteres = resultSet.getInt("idTipoInteres");
                objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getBoolean("estado"),
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objTasaInteres != null) {
            objTasaInteres.setObjTipoInteres(DAOTipoInteres.getInstance().getTipoInteres(idTipoInteres));
        }

        return objTasaInteres;
    }

    @Override
	public TasaInteres getTasaInteres(TipoInteres objTipoInteres) {
    	TasaInteres objTasaInteres = null;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.estado, "
                + "ti.tea, ti.tim, ti.fechaRegistro, ti.idTipoInteres "
                + "FROM TasasInteres ti "
                + "WHERE ti.id IN (SELECT MAX(tisub.id) id "
                + "			FROM TasasInteres tisub "
                + "			WHERE tisub.idTipoInteres = ?);")) {
            preparedStatement.setInt(1, objTipoInteres.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getBoolean("estado"),
                        objTipoInteres);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objTasaInteres;
	}

	@Override
	public List<TasaInteres> getAllTasasInteres(Usuario objUsuario) {
		List<TasaInteres> lstTasasInteres = new ArrayList<>();
		List<Integer> lstIdsTiposInteres = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.estado, "
        		+ "ti.tea, ti.tim, ti.fechaRegistro, ti.idTipoInteres "
        		+ "FROM TasasInteres ti INNER JOIN (SELECT MAX(taisub.id) idTasaInteres, tiisub.nombre "
        		+ "FROM TasasInteres taisub INNER JOIN TiposInteres tiisub ON tiisub.id = taisub.idTipoInteres "
        		+ "WHERE tiisub.idUsuario = ? "
        		+ "GROUP BY tiisub.nombre) tai ON tai.idTasaInteres = ti.id;")) {
        	preparedStatement.setInt(1, objUsuario.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
            	lstIdsTiposInteres.add(resultSet.getInt("idTipoInteres"));
            	
                TasaInteres objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getBoolean("estado"),
                        null);
                lstTasasInteres.add(objTasaInteres);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(!lstTasasInteres.isEmpty()){
        	DAOTipoInteres objDAOTipoInteres = DAOTipoInteres.getInstance();
        	
        	int size = lstTasasInteres.size();
        	for(int i = 0; i < size; i++){
        		lstTasasInteres.get(i).setObjTipoInteres(objDAOTipoInteres.getTipoInteres(lstIdsTiposInteres.get(i)));
        	}
        }

        return lstTasasInteres;
	}

	@Override
    public List<TasaInteres> getAllTasasInteres(TipoInteres objTipoInteres) {
        List<TasaInteres> lstTasasInteres = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.estado, "
                + "ti.tea, ti.tim, ti.fechaRegistro, ti.idTipoInteres "
                + "FROM TasasInteres ti "
                + "WHERE ti.idTipoInteres = ?;")) {
            preparedStatement.setInt(1, objTipoInteres.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                TipoInteres objTipoInteresReplace = new TipoInteres(
                        objTipoInteres.getId(),
                        objTipoInteres.getNombre(),
                        objTipoInteres.getDiasPlazos(),
                        objTipoInteres.isEstado(),
                        objTipoInteres.getFechaRegistro(),
                        objTipoInteres.getFechaModificacion());
                TasaInteres objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getBoolean("estado"),
                        objTipoInteresReplace);
                lstTasasInteres.add(objTasaInteres);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstTasasInteres;
    }

	@Override
	public boolean insert(TasaInteres objTasaInteres) {
		boolean result = false;
		
		try(CallableStatement callableStatement = ConnectionManager.getConnection().prepareCall("{? = call sp_UpdateInterestType(?, ?, ?, ?, ?)}")){
			callableStatement.registerOutParameter(1, Types.INTEGER);
			callableStatement.setString(2, objTasaInteres.getObjTipoInteres().getNombre());
			callableStatement.setInt(3, objTasaInteres.getObjTipoInteres().getDiasPlazos());
			callableStatement.setBigDecimal(4, objTasaInteres.getTea());
			callableStatement.setBigDecimal(5, objTasaInteres.getTim());
			callableStatement.setInt(6, objTasaInteres.getObjTipoInteres().getId());
			
			callableStatement.execute();
			
			objTasaInteres.setId(callableStatement.getInt(1));
			
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
}
