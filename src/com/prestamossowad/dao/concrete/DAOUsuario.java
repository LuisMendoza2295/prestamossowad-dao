package com.prestamossowad.dao.concrete;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOUsuario;
import com.prestamossowad.dominio.Usuario;

public class DAOUsuario implements IDAOUsuario{

private static DAOUsuario instance = null;
    
    private DAOUsuario(){
    }
    
    public static DAOUsuario getInstance(){
        if(instance == null){
            instance = new DAOUsuario();
        }
        
        return instance;
    }
    
    public Usuario getUsuario(int id){
        Usuario objUsuario = null;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT u.id, u.username, u.pass, "
                + "u.nombre, u.apellidopaterno, u.apellidomaterno, u.dni, u.estado, u.fecharegistro, u.fechamodificacion "
                + "FROM usuarios u "
                + "WHERE u.id = ?")){
            preparedStatement.setInt(1, id);
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                objUsuario = new Usuario(
                        id,
                        resultSet.getString("dni"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getString("username"),
                        resultSet.getString("pass"),
                        resultSet.getDate("fecharegistro"),
                        resultSet.getDate("fechamodificacion"),
                        resultSet.getBoolean("estado"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(objUsuario != null){
            objUsuario.setLstTiposInteres(DAOTipoInteres.getInstance().getAllTiposInteres(objUsuario));
        }
        
        return objUsuario;
    }
    
    public Usuario getUsuario(String username, String password){
        Usuario objUsuario = new Usuario();
        
        try (CallableStatement callableStatement = ConnectionManager.getConnection().prepareCall("{call sp_verifyAccess(?, ?)}")){
            
            callableStatement.setString(1, username);
            callableStatement.setString(2, password);
            
            ResultSet resultSet = callableStatement.executeQuery();
            
            while(resultSet.next()){
                objUsuario = new Usuario(
                        resultSet.getInt("id"),
                        resultSet.getString("dni"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"), 
                        username, 
                        password, 
                        resultSet.getDate("fecharegistro"),
                        resultSet.getDate("fechamodificacion"),
                        resultSet.getBoolean("estado"));
                
            }
            
        } catch (Exception e) {
                e.printStackTrace();
        }
        
        if(objUsuario != null){
        	objUsuario.setLstTiposInteres(DAOTipoInteres.getInstance().getAllTiposInteres(objUsuario));
        }
        
        return objUsuario;
    }

    public String insert(Usuario objUsuario){
        try (CallableStatement callableStatement = ConnectionManager.getConnection().prepareCall("{? = call sp_RegisterUser(?, ?, ?, ?, ?, ?)}")){
            //int id = 0;
            objUsuario.setEstado(true);
            
            callableStatement.registerOutParameter(1, Types.INTEGER);
            callableStatement.setString(2, objUsuario.getNombre());
            callableStatement.setString(3, objUsuario.getApellidoPaterno());
            callableStatement.setString(4, objUsuario.getApellidoMaterno());
            callableStatement.setString(5, objUsuario.getDni());
            callableStatement.setString(6, objUsuario.getUsername());
            callableStatement.setString(7, objUsuario.getPassword());    
            //callableStatement.setInt(8, id);
            
            callableStatement.execute();
            
            objUsuario.setId(callableStatement.getInt(1));
            
            return "Usuario registrado correctamente!";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

	@Override
	public List<Usuario> getAllUsuarios(Usuario objUsuario) {
		List<Usuario> lstUsuarios = new ArrayList<>();
		
		try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT u.id, u.username, u.pass, "
                + "u.nombre, u.apellidopaterno, u.apellidomaterno, u.dni, u.estado, u.fecharegistro, u.fechamodificacion "
                + "FROM usuarios u "
                + "WHERE u.id <> ?")){
            preparedStatement.setInt(1, objUsuario.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                Usuario objUsuarioResult = new Usuario(
                        resultSet.getInt("id"),
                        resultSet.getString("dni"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getString("username"),
                        resultSet.getString("pass"),
                        resultSet.getDate("fecharegistro"),
                        resultSet.getDate("fechamodificacion"),
                        resultSet.getBoolean("estado"));
                lstUsuarios.add(objUsuarioResult);                
            }
        }catch(Exception e){
            e.printStackTrace();
        }
		
		if(!lstUsuarios.isEmpty()){
			DAOTipoInteres objDAOTipoInteres = DAOTipoInteres.getInstance();
			
			int size = lstUsuarios.size();
			for(int i = 0; i < size; i++){
				lstUsuarios.get(i).setLstTiposInteres(objDAOTipoInteres.getAllTiposInteres(lstUsuarios.get(i)));
			}
		}
		
		return lstUsuarios;
	}
}
