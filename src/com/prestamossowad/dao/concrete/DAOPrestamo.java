package com.prestamossowad.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOPrestamo;
import com.prestamossowad.dominio.Cuota;
import com.prestamossowad.dominio.Prestamo;
import com.prestamossowad.dominio.Usuario;

public class DAOPrestamo implements IDAOPrestamo{
	
	private static DAOPrestamo instance = null;
	
	private DAOPrestamo(){
	}
	
	public static DAOPrestamo getInstance(){
		if(instance == null){
			instance = new DAOPrestamo();
		}
		
		return instance;
	}

	@Override
	public Prestamo getPrestamo(int id) {
		Prestamo objPrestamo = null;
		int idSolicitudPrestamo = 0;
		
		try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.tem, p.pagado, "
				+ "p.fechaRegistro, p.fechaModificacion, p.estado, p.idSolicitudPrestamo "
				+ "FROM Prestamos p "
				+ "WHERE p.id = ?;")){
			preparedStatement.setInt(1, id);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()){
				idSolicitudPrestamo = resultSet.getInt("idSolicitudPrestamo");
				
				objPrestamo = new Prestamo(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getTimestamp("fechaRegistro"),
                        resultSet.getTimestamp("fechaModificacion"),
                        null,
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("estado"),
                        new ArrayList<>());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(objPrestamo != null){
			objPrestamo.setObjSolicitudPrestamo(DAOSolicitudPrestamo.getInstance().getSolicitudPrestamo(idSolicitudPrestamo));
			objPrestamo.setLstCuotas(DAOCuota.getInstance().getAllCuotas(objPrestamo));
		}
		
		return objPrestamo;
	}

	@Override
	public Prestamo getPrestamo(Cuota objCuota) {
		Prestamo objPrestamo = null;
        int idSolicitudPrestamo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.tem, p.pagado, p.fechaRegistro, "
                + "p.fechaModificacion, p.estado, p.idSolicitudPrestamo "
                + "FROM Prestamos p "
                + "WHERE id IN (SELECT c.idPrestamo "
                + "             FROM Cuotas c "
                + "             WHERE c.id = ?);")) {

            preparedStatement.setInt(1, objCuota.getId());

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                idSolicitudPrestamo = resultSet.getInt("idSolicitudPrestamo");

                objPrestamo = new Prestamo(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getDate("fechaRegistro"),
                        resultSet.getDate("fechaModificacion"),
                        null,
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("estado"),
                        new ArrayList<>());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objPrestamo != null) {
            objPrestamo.setObjSolicitudPrestamo(DAOSolicitudPrestamo.getInstance().getSolicitudPrestamo(idSolicitudPrestamo));
            objPrestamo.setLstCuotas(DAOCuota.getInstance().getAllCuotas(objPrestamo));
        }

        return objPrestamo;
	}

	@Override
	public List<Prestamo> getAllPrestamosActivos(Usuario objUsuario, boolean prestamista) {
		List<Prestamo> lstPrestamos = new ArrayList<>();
        List<Integer> lstIdsSolicitudesPrestamo = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.tem, p.pagado, p.fechaRegistro, p.fechaModificacion, p.estado, p.idSolicitudPrestamo "
                + "FROM Prestamos p INNER JOIN SolicitudesPrestamo sp ON sp.id = p.idSolicitudPrestamo "
                + "WHERE sp.id in (SELECT spsub.id "
                + "FROM SolicitudesPrestamo spsub INNER JOIN DetalleSolicitudPrestamo dspsub ON spsub.id = dspsub.idSolicitudPrestamo "
                + "INNER JOIN Usuarios usub ON usub.id = dspsub.idUsuario "
                + "WHERE dspsub.idUsuario = ? AND dspsub.prestamista = ?) AND p.estado = 1 AND p.pagado = 0;")) {
            preparedStatement.setInt(1, objUsuario.getId());
            preparedStatement.setBoolean(2, prestamista);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsSolicitudesPrestamo.add(resultSet.getInt("idSolicitudPrestamo"));

                Prestamo objPrestamo = new Prestamo(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getTimestamp("fechaRegistro"),
                        resultSet.getTimestamp("fechaModificacion"),
                        null,
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("estado"),
                        new ArrayList<>());
                lstPrestamos.add(objPrestamo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPrestamos.isEmpty()) {
            DAOSolicitudPrestamo objDAOSolicitudPrestamo = DAOSolicitudPrestamo.getInstance();
            DAOCuota objDAOCuota = DAOCuota.getInstance();

            int size = lstPrestamos.size();
            for (int i = 0; i < size; i++) {
                lstPrestamos.get(i).setObjSolicitudPrestamo(objDAOSolicitudPrestamo.getSolicitudPrestamo(lstIdsSolicitudesPrestamo.get(i)));
                lstPrestamos.get(i).setLstCuotas(objDAOCuota.getAllCuotas(lstPrestamos.get(i)));
            }
        }

        return lstPrestamos;
	}

	@Override
	public boolean insert(Prestamo objPrestamo) {
		boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO prestamos "
                + "(tem, pagado, fechaRegistro, fechaModificacion, estado, idSolicitudPrestamo) "
                + "VALUES(?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setBigDecimal(1, objPrestamo.getTem());
            preparedStatement.setBoolean(2, objPrestamo.isPagado());
            preparedStatement.setDate(3, new java.sql.Date(objPrestamo.getFechaRegistro().getTime()));
            if (objPrestamo.getFechaModificacion() == null) {
                preparedStatement.setNull(4, Types.DATE);
            } else {
                preparedStatement.setDate(4, new java.sql.Date(objPrestamo.getFechaModificacion().getTime()));
            }
            preparedStatement.setBoolean(5, objPrestamo.isEstado());
            preparedStatement.setInt(6, objPrestamo.getObjSolicitudPrestamo().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();
            if (keySet.next()) {
                objPrestamo.setId(keySet.getInt(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
	}
}
