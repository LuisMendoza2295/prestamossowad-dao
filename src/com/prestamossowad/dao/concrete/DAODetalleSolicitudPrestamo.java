package com.prestamossowad.dao.concrete;

import java.sql.PreparedStatement;
import java.util.List;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAODetalleSolicitudPrestamo;
import com.prestamossowad.dominio.DetalleSolicitudPrestamo;

public class DAODetalleSolicitudPrestamo implements IDAODetalleSolicitudPrestamo{

	private static DAODetalleSolicitudPrestamo instance = null;
	
	private DAODetalleSolicitudPrestamo(){
	}
	
	public static DAODetalleSolicitudPrestamo getInstance(){
		if(instance == null){
			instance = new DAODetalleSolicitudPrestamo();
		}
		
		return instance;
	}

	@Override
	public boolean insertDetalleSolicitudPrestamo(List<DetalleSolicitudPrestamo> lstDetalleSolicitudPrestamo) {
		boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO DetalleSolicitudPrestamo "
                + "(prestamista, idUsuario, idSolicitudPrestamo) "
                + "VALUES(?, ?, ?);")) {

            for (DetalleSolicitudPrestamo objDetalleSolicitudPrestamo : lstDetalleSolicitudPrestamo) {
                preparedStatement.setBoolean(1, objDetalleSolicitudPrestamo.isPrestamista());
                preparedStatement.setInt(2, objDetalleSolicitudPrestamo.getObjUsuario().getId());
                preparedStatement.setInt(3, objDetalleSolicitudPrestamo.getObjSolicitudPrestamo().getId());
                
                preparedStatement.execute();
            }
            
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
	}
}
