package com.prestamossowad.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOPago;
import com.prestamossowad.dominio.Pago;

public class DAOPago implements IDAOPago{

	private static DAOPago instance = null;
	
	private DAOPago(){
	}
	
	public static DAOPago getInstance(){
		if(instance == null){
			instance = new DAOPago();
		}
		
		return instance;
	}

	@Override
	public Pago getPago(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean insert(Pago objPago) {
		boolean resultado = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO Pagos (monto, fecha, idCuota, idTipoPago) "
        		+ "VALUES (?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS)){
            
            preparedStatement.setBigDecimal(1, objPago.getMonto());
            preparedStatement.setDate(2, new java.sql.Date(objPago.getFecha().getTime()));
            preparedStatement.setInt(3, objPago.getObjCuota().getId());
            preparedStatement.setInt(4, objPago.getObjTipoPago().getId());
            
            resultado = preparedStatement.execute();
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            
            while(keySet.next()){
                objPago.setId(keySet.getInt(1));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado; 
	}
}
