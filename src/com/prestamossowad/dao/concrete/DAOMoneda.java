package com.prestamossowad.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOMoneda;
import com.prestamossowad.dominio.Moneda;

public class DAOMoneda implements IDAOMoneda{

	private static DAOMoneda instance = null;
	
	private DAOMoneda(){
	}
	
	public static DAOMoneda getInstance(){
		if(instance == null){
			instance = new DAOMoneda();
		}
		
		return instance;
	}

	@Override
	public Moneda getMoneda(int id) {
		Moneda objMoneda = null;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT g.id, g.nombre, g.abreviatura, tc.valorActual "
                + "FROM (SELECT m.id, m.nombre, m.abreviatura, MAX(tc.idTipoCambioSub) as idTipoCambio "
                + "         FROM Monedas m INNER JOIN (SELECT MAX(id) idTipoCambioSub, idMoneda "
                + "					FROM TiposCambio "
                + "					GROUP BY idMoneda) tc ON m.id = tc.idMoneda "
                + "         GROUP BY m.id, m.nombre, m.abreviatura) g INNER JOIN TiposCambio tc ON g.idTipoCambio = tc.id "
                + "WHERE g.id = ?;")) {
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                objMoneda = new Moneda(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("abreviatura"),
                        resultSet.getBigDecimal("valorActual"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objMoneda;
	}

	@Override
	public List<Moneda> getAllMonedas() {
		List<Moneda> lstMonedas = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT g.id, g.nombre, g.abreviatura, tc.valorActual "
                + "FROM (SELECT m.id, m.nombre, m.abreviatura, MAX(tc.idTipoCambioSub) as idTipoCambio "
                + "         FROM Monedas m INNER JOIN (SELECT MAX(id) idTipoCambioSub, idMoneda "
                + "					FROM TiposCambio "
                + "					GROUP BY idMoneda) tc ON m.id = tc.idMoneda "
                + "         GROUP BY m.id, m.nombre, m.abreviatura) g INNER JOIN TiposCambio tc ON g.idTipoCambio = tc.id;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Moneda objMoneda = new Moneda(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("abreviatura"),
                        resultSet.getBigDecimal("valorActual"));

                lstMonedas.add(objMoneda);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstMonedas;
	}
}
