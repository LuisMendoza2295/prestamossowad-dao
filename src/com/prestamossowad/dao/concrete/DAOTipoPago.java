package com.prestamossowad.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.prestamossowad.connection.ConnectionManager;
import com.prestamossowad.dao.interfaces.IDAOTipoPago;
import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.TipoPago;

public class DAOTipoPago implements IDAOTipoPago{

	private static DAOTipoPago instance = null;

    private DAOTipoPago() {
    }

    public static DAOTipoPago getInstance() {
        if (instance == null) {
            instance = new DAOTipoPago();
        }

        return instance;
    }
    
    @Override
    public boolean insert(TipoPago objTipoPago){
        boolean resultado = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO TiposPago (nombre) VALUES(?)", PreparedStatement.RETURN_GENERATED_KEYS)){
            
            preparedStatement.setString(1, objTipoPago.getNombre());
            
            resultado = preparedStatement.execute();
            
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            
            while(keySet.next()){
                objTipoPago.setId(keySet.getInt(1));
            }
            
            
        } catch (Exception e) {
        }
        return resultado;
    }
    
    @Override
    public TipoPago getTipoPago(int id){
        TipoPago objTipoPago = null;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tp.id, tp.nombre "
                + "FROM TiposPago tp "
                + "WHERE tp.id = ?;")) {
            preparedStatement.setInt(1, id);
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                objTipoPago = new TipoPago(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return objTipoPago;
    }

    public List<TipoPago> getAllTiposPago(SolicitudPrestamo objSolicitudPrestamo) {
        List<TipoPago> lstTiposPago = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tp.id, tp.nombre "
                + "FROM TiposPago tp INNER JOIN DetalleTiposPago dtp ON tp.id = dtp.idTipoPago "
                + "INNER JOIN SolicitudesPrestamo sp ON sp.id = dtp.idSolicitudPrestamo "
                + "WHERE sp.id = ?;")) {
            preparedStatement.setInt(1, objSolicitudPrestamo.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                TipoPago objTipoPago = new TipoPago(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"));
                lstTiposPago.add(objTipoPago);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstTiposPago;
    }    
}
